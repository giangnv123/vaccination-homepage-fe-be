"use strict";

const contactUrl = `/api/v1/vaccine/contacts/`;

var vContactTable = "";
var vStt = 1;
var vCurrentContactId = "";

// CRUD
const CREATE = "create";
const UPDATE = "update";
const DELETE = "delete";

$(function () {
  createContactTable();
});

async function getAllContacts() {
  const response = await fetch(contactUrl);
  const data = await response.json();
  return data.data.contacts;
}

/******** Funtions help load Data To Table ********/

async function createContactTable() {
  console.log("Create Table");
  vContactTable = $("#contact-table").DataTable({
    columns: [
      {
        defaultContent: 1,
        render: () => vStt++,
      },
      {
        data: "email",
      },
      {
        data: "createdAt",
        render: (data) => new Date(Number(data)).toLocaleDateString(),
      },
      {
        data: null,
        render: renderButtons,
      },
    ],
    columnDefs: [{ targets: "_all", class: "text-center" }],
  });
  await loadContact();
}

async function getAllContact() {
  const response = await fetch(contactUrl);
  const data = await response.json();
  const allContacts = data.data.contacts;
  return allContacts;
}

async function loadContact() {
  const allContacts = await getAllContact();
  vStt = 1;
  vContactTable.clear();
  vContactTable.rows.add(allContacts);
  vContactTable.draw();
}

function renderButtons(paramData) {
  var vBtnEdit = `<i data-contact='${JSON.stringify(paramData)}' class="edit-contact fa-solid fa-pen-to-square fa-lg
      text-primary contact-select-all mr-2" style="cursor:pointer;"></i>`;
  var vBtnDelete = `<i data-id='${paramData._id}' class="delete-contact fa-regular fa-trash-can fa-lg
      text-danger contact-select-all" style="cursor:pointer;"></i>`;
  return `${vBtnEdit} ${vBtnDelete}`;
}

/******** Event Handler ********/

// Update Contact
$("#contact-table").on("click", ".edit-contact", function () {
  var vContactObj = JSON.parse(this.dataset.contact);
  handleUpdateBtn(vContactObj);
});

$("#btn-update-contact").click(() => handleCrud(UPDATE));

// Delete Contact
$("#contact-table").on("click", ".delete-contact", function () {
  vCurrentContactId = this.dataset.id;
  $("#delete-modal").modal("show");
});

$("#btn-delete-contact").click(() => handleCrud(DELETE));

// Create Contact
$("#btn-create").click(handleBtnAddContact);

$("#btn-create-contact").click(() => handleCrud(CREATE));

/******** Call Api Function Ajax ********/

// Create Contact
function callApiToCreateContact(paramObj) {
  $.ajax({
    url: `${contactUrl}`,
    method: "POST",
    contentType: "application/json",
    data: JSON.stringify(paramObj),
    success: function (res) {
      console.log(res);
      handleSuccessState(CREATE);
    },
  });
}

// Update Contact
function callApiToUpdateContact(paramObj) {
  $.ajax({
    url: `${contactUrl}${vCurrentContactId}`,
    method: "PATCH",
    contentType: "application/json",
    data: JSON.stringify(paramObj),
    success: function (res) {
      console.log(res);
      handleSuccessState(UPDATE);
    },
  });
}

// Delete Contact
function callApiToDeleteContact() {
  $.ajax({
    url: `${contactUrl}${vCurrentContactId}`,
    method: "DELETE",
    success: function () {
      handleSuccessState(DELETE);
    },
  });
}

/******** CRUD Function ********/

function handleCrud(paramMethod) {
  if (paramMethod === DELETE) {
    return callApiToDeleteContact();
  }

  var vContactObj = getEmptyContactObject();

  getDataFromModal(paramMethod, vContactObj);

  if (!validateInput(vContactObj, paramMethod)) return;

  console.log(vContactObj);
  paramMethod === CREATE ? callApiToCreateContact(vContactObj) : callApiToUpdateContact(vContactObj);
}

function getEmptyContactObject() {
  return {
    email: "",
    createdAt: Date.now(),
  };
}

/******** Functions help Create Contact ********/
function handleBtnAddContact() {
  $("#create-modal").modal(`show`);
}

/******** Functions help Update Contact ********/
function handleUpdateBtn(paramContact) {
  $("#update-modal").modal("show");
  vCurrentContactId = paramContact._id;
  populateDataToModalUpdate(paramContact);
}

function populateDataToModalUpdate(paramObj) {
  $("#inp-update-email").val(paramObj.email);
}

/******** Support Functions to Create and Update Contact ********/
function getDataFromModal(paramMethod, paramObj) {
  paramObj.email = $(`#inp-${paramMethod}-email`).val().trim();
}

function validateInput(paramObj, paramMethod) {
  if (!paramObj.email) {
    alert(`Please fill out all input!`);
  } else if (!validateEmail(paramObj.email)) {
    alert("Invalid Email! Try again");
  } else {
    return true;
  }
  return false;
}

function handleSuccessState(paramMethod) {
  alert(`${paramMethod.toUpperCase()} Contact Successfully!`);
  $(`#${paramMethod}-modal`).modal("hide");
  $(`#${paramMethod}-form`).trigger("reset");
  loadContact();
}

function validateEmail(email) {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailRegex.test(email);
}
