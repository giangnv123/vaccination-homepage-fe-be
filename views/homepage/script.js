"use strict";

const contactUrl = `/api/v1/vaccine/contacts/`;
const userUrl = `/api/v1/vaccine/users/`;
let allUsers = [];

$(document).ready(() => {
  $("#contact-form").submit(function (e) {
    e.preventDefault();
    handleOnSubmitContact();
  });

  $("#regis-form").submit(function (e) {
    e.preventDefault();
    handleOnSubmitRegis();
  });

  updateAllUsers();
});

async function updateAllUsers() {
  allUsers = await getAllUsers();
}

async function getAllUsers() {
  const response = await fetch(userUrl);
  const data = await response.json();
  console.log(data.data.users);
  return data.data.users;
}

/**** Main function - Handle Submit Form****/

async function handleOnSubmitRegis() {
  let regisObj = {
    fullName: "",
    phone: Number,
  };

  getDataToRegisForm(regisObj);

  if (!validateRegisInput(regisObj)) return;

  await callApiToCreateNewRegis(regisObj);
}

async function handleOnSubmitContact() {
  let contactObj = {
    email: "",
  };

  getEmailFromInput(contactObj);

  if (!validateContact(contactObj)) return;

  await callApiToCreateNewContact(contactObj);
}

/**** Call Api Function ****/

async function callApiToCreateNewContact(contactObj) {
  try {
    const response = await fetch(contactUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(contactObj),
    });

    if (response.ok) {
      const data = await response.json();
      handleSuccessCreateContact(data);
    } else {
      console.log("Failed to create a new contact");
    }
  } catch (error) {
    console.error("Error:", error);
  }
}

async function callApiToCreateNewRegis(regisObj) {
  try {
    const response = await fetch(userUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(regisObj),
    });

    if (response.ok) {
      const data = await response.json();
      handleSuccessCreateRegis(data);
    } else {
      console.log("Failed to create a registration");
    }
  } catch (error) {
    console.error("Error:", error);
  }
}

/**** Handle Check phone number btn ****/
$("#btn-verify").click(() => (handleCheckPhoneNumber() ? alert("Phone Number Is Okay!") : ""));

function handleCheckPhoneNumber() {
  const phoneNumber = $("#inp-phone").val().trim();
  const isExistingPhoneNumber = checkExistingPhoneNumber(phoneNumber);
  if (!phoneNumber) alert("Please enter your phone number");
  else if (!validatePhoneNumber(phoneNumber)) {
    alert("Invalid phone number");
  } else if (isExistingPhoneNumber) {
    alert("Existing Phone Number, Please Try Another One!");
  } else {
    return true;
  }
  return false;
}

function checkExistingPhoneNumber(phoneNumber) {
  const existingPhoneNumber = allUsers.find((user) => user.phone == phoneNumber);
  return existingPhoneNumber || false;
}

/**** Helper - Handle Success ****/

function handleSuccessCreateContact(data) {
  console.log("New contact created:", data);
  alert("Your email has been sent!");
  $("#form-contact").trigger("reset");
}

function handleSuccessCreateRegis(data) {
  console.log("New regis created:", data);
  alert("Create new registration successfully!");
  $("#regis-form").trigger("reset");
  updateAllUsers();
}

/**** Helper Function - Get Data ****/

function getDataToRegisForm(regisObj) {
  regisObj.fullName = $("#inp-name").val().trim();
  regisObj.phone = $("#inp-phone").val().trim();
}

function getEmailFromInput(contactObj) {
  contactObj.email = $("#inp-email").val().trim();
}

/**** Helper Function - Validation ****/

function validateContact(contactObj) {
  if (!contactObj.email) alert("Please Enter your email!");
  else if (!validateEmail(contactObj.email)) {
    alert("Invalid Email!");
  } else {
    return true;
  }
  return false;
}

function validateRegisInput(regisObj) {
  if (!regisObj.fullName || !regisObj.phone) {
    alert("Please enter both name and phone number!");
  } else if (!handleCheckPhoneNumber(regisObj.phone)) {
  } else {
    return true;
  }
  return false;
}

function validateEmail(email) {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailRegex.test(email);
}

function validatePhoneNumber(phoneNumber) {
  const phoneNumberRegex = /^0\d{9,10}$/;
  return phoneNumberRegex.test(phoneNumber);
}

// Nav toggle
$(".nav-toggle").click(function () {
  $(this).toggleClass("show-toggle");
  $(".nav-content").toggleClass(`show-nav`);
});
