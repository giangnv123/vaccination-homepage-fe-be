const mongoose = require("mongoose");
const express = require("express");
const path = require("path");

// Get Routers
const { userRouter } = require("./app/routers/userRouter");
const { contactRouter } = require("./app/routers/contactRouter");

const app = express();
const port = 8000;

app.use(express.json());

// View User Page
app.use(express.static(__dirname + "/views/homepage"));

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname + "/views/homepage/index.html"));
});

app.get("/admin/users", (req, res) => {
  res.sendFile(path.join(__dirname, "views", "admin", "user", "admin.html"));
});

app.get("/admin/contacts", (req, res) => {
  res.sendFile(path.join(__dirname, "views", "admin", "contact", "admin.html"));
});
// Connect to mongoDb Local Database
const mongoUrl = `mongodb://127.0.0.1:27017/CRUD_Vaccine`;
mongoose.connect(mongoUrl).then(() => console.log(`DB Connection to Local Database Successfully!`));

app.use("/api/v1/vaccine/users", userRouter);
app.use("/api/v1/vaccine/contacts", contactRouter);

app.listen(port, () => console.log(`App is running on port: ${port}`));
