const express = require("express");

const { getUsers, getUser, createUser, updateUser, deleteUser } = require("../controllers/userController");

/***** ROUTER *****/

// Create router
const userRouter = express.Router();

userRouter.route("/").get(getUsers).post(createUser);

userRouter.route("/:id").patch(updateUser).get(getUser).delete(deleteUser);

// Export user Router
module.exports = { userRouter };
