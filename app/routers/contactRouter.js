const express = require("express");

const {
  getContacts,
  getContact,
  createContact,
  updateContact,
  deleteContact,
} = require("../controllers/contactController");

/***** ROUTER *****/

// Create router
const contactRouter = express.Router();

contactRouter.route("/").get(getContacts).post(createContact);

contactRouter.route("/:id").patch(updateContact).get(getContact).delete(deleteContact);

// Export contact Router
module.exports = { contactRouter };
