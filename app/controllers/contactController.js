const Contact = require("../models/contact-model");

// GET ContactS

exports.getContacts = async (req, res) => {
  try {
    const contacts = await Contact.find();

    // SEND RESPONSE
    res.status(200).json({
      message: "Success",
      result: contacts.length,
      data: {
        contacts,
      },
    });
  } catch (error) {
    res.status(404).json({
      message: "Fail to get Contact!",
      error,
    });
  }
};

// Get one single contact

exports.getContact = async (req, res) => {
  try {
    const contact = await Contact.findById(req.params.id);
    res.status(200).json({
      message: "Success",
      data: {
        contact,
      },
    });
  } catch (error) {
    res.status(404).json({
      message: "Contact Not Found!",
      error,
    });
  }
};

// CREATE Contact

exports.createContact = async (req, res) => {
  try {
    const contact = await Contact.create(req.body);
    res.status(200).json({
      message: "Success",
      data: {
        contact,
      },
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to create new contact!",
      error,
    });
  }
};

// UPDATE Contact

exports.updateContact = async (req, res) => {
  try {
    const contact = await Contact.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });
    res.status(200).json({
      message: "Success",
      data: {
        contact,
      },
    });
  } catch (error) {
    res.status(404).json({
      message: "Fail to update contact!",
      error,
    });
  }
};

// DELETE Contact
exports.deleteContact = async (req, res) => {
  try {
    await Contact.findByIdAndDelete(req.params.id);
    res.status(204).json({
      message: "Success",
    });
  } catch (error) {
    res.status(404).json({
      message: "Fail to delete contact!",
      error,
    });
  }
};
