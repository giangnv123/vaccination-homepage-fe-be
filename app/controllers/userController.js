const User = require("../models/user-model");

// GET UserS

exports.getUsers = async (req, res) => {
  try {
    const users = await User.find();

    // SEND RESPONSE
    res.status(200).json({
      message: "Success",
      result: users.length,
      data: {
        users,
      },
    });
  } catch (error) {
    res.status(404).json({
      message: "Fail to get User!",
      error,
    });
  }
};

// Get one single user

exports.getUser = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    res.status(200).json({
      message: "Success",
      data: {
        user,
      },
    });
  } catch (error) {
    res.status(404).json({
      message: "User Not Found!",
      error,
    });
  }
};

// CREATE User

exports.createUser = async (req, res) => {
  try {
    const user = await User.create(req.body);
    res.status(200).json({
      message: "Success",
      data: {
        user,
      },
    });
  } catch (error) {
    res.status(400).json({
      message: "Fail to create new user!",
      error,
    });
  }
};

// UPDATE User

exports.updateUser = async (req, res) => {
  try {
    const user = await User.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });
    res.status(200).json({
      message: "Success",
      data: {
        user,
      },
    });
  } catch (error) {
    res.status(404).json({
      message: "Fail to update user!",
      error,
    });
  }
};

// DELETE User
exports.deleteUser = async (req, res) => {
  try {
    await User.findByIdAndDelete(req.params.id);
    res.status(204).json({
      message: "Success",
    });
  } catch (error) {
    res.status(404).json({
      message: "Fail to delete user!",
      error,
    });
  }
};
