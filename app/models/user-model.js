const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  fullName: {
    type: String,
    required: [true, "Please provide the full name."],
  },
  phone: {
    type: String,
    required: [true, "Please provide the phone number."],
    unique: true,
  },
  status: {
    type: String,
    default: "Level 0",
  },
  createdAt: {
    type: String,
    default: Date.now(),
  },
});

const User = mongoose.model("User", UserSchema);

module.exports = User;
