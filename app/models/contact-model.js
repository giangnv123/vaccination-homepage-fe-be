const mongoose = require("mongoose");

const ContactSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, "Please provide an email address."],
  },
  createdAt: {
    type: String,
    default: Date.now(),
  },
});

const Contact = mongoose.model("Contact", ContactSchema);

module.exports = Contact;
